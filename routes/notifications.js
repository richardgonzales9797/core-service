const express = require("express");
const router = express.Router();
const {teacherNotifications, studentNotifications, addNotifications, updateStatus} = require("../controllers/notifications.controller");

router.post("/", addNotifications);
router.get("/teachers/:id", teacherNotifications);
router.get("/students/:id", studentNotifications);
router.put("/:id", updateStatus);

module.exports = router;